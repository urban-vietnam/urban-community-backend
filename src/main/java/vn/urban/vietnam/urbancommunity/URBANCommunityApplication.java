package vn.urban.vietnam.urbancommunity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class URBANCommunityApplication {

	public static void main(String[] args) {
		SpringApplication.run(URBANCommunityApplication.class, args);
	}

}
